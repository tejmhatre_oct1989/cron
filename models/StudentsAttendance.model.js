import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    studentId:{
        type:String,
        required:true,
        lowercase:true
    },
    date:{
        type:String,
        required:true
    },
    attended:{
        type:String,
        enum : ['0','1','2'],
        default: '0'
    },
    cronStatus:{
        type:String,
        enum : ['0','1','2'],
        default: '0'
    }
});

export default mongoose.model('studentAttendance',UserSchema);