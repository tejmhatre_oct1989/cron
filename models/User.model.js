import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    studentId:{
        type:String,
        required:true,
        lowercase:true,
        unique:true
    },
    name:{
        type:String,
        required:true
    },
    rollNo:{
        type:String,
        required:true
    },
    standard:{
        type:String,
        required:true
    },
    div:{
        type:String,
        required:true
    }
});

export default mongoose.model('user',UserSchema);