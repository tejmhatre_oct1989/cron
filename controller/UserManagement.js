import createError from 'http-errors';
import User from '../models/User.model';
import studentAttendance from '../models/StudentsAttendance.model';
const fs = require('fs')
const csv = require('csv-parser')
const stringify = require('csv-stringify')
const { fork } = require('child_process');  
import { students } from '../helpers/students'


export async function uploadstudents(req,res,next){
    try {
        students(req.file.path,{'model':User},(status) => {
            if(!status){
                throw createError.BadGateway();
            }
            return  res.status(200).json({
                'message':'students uploaded',
             });
        })

    } catch (error) {
       next(error);
    }
}


export async function uploadstudentsAttendance(req,res,next){
    try {

        students(req.file.path,{'model':studentAttendance},(status) => {
            if(!status){
                throw createError.BadGateway();
            }
            return  res.status(200).json({
                'message':'students uploaded',
             });
        })

    } catch (error) {
       next(error);
    }
}

export async function cronForCsv(req,res,next){
    try {
        const child = fork(process.cwd()+'/cron/attendance.js');
        child.send({ hello: 'from parent process' });
        child.on('message', function(m) {
            console.log('Parent process received:', m);
          });
          child.on('close', (code) => {
            console.log(`child process exited with code ${code}`);
          });
    } catch (error) {
        console.log('err',error)
    }
   
}


export async function cronForCsvMail(req,res,next){
    const child = fork(process.cwd()+'/cron/attendanceMail.js');
    child.send({ hello: 'from parent process' });
    child.on('message', function(m) {
        console.log('Parent process received:', m);
      });

      child.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
      });
}

export async function studentsAttendence(req,res,next){
    try {
        var someData = [];

        stringify(someData, {       
            header: true
        }, function (err, output) {
            if(err) throw createError.BadGateway();
            fs.writeFile(process.cwd()+'/public/attendance.csv', output,(err) => {
                if(err) throw createError.BadGateway();
            else {  
                    return  res.status(200).json({
                        'message':'students uploaded',
                     });
            }
            });
        });
    } catch (error) {
       next(error);
    }
}