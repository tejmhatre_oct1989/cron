import {} from '../helpers/init_mongodb';
import studentAttendance from '../models/StudentsAttendance.model';
import { apiErrLog } from '../config/errFun'
import config from './config/config';

const fs = require('fs')
const stringify = require('csv-stringify')
 function attendanceCron(){
    var date = config.date();
    let today = date.dd+date.mm+date.yyyy;    
    try{
        studentAttendance.
                aggregate([
                    {
                    $lookup:{
                        from: "users",
                        let: { studentId: "$studentId"},
                        pipeline: [
                             { $match:
                                { 
                                    $expr:{
                                        $eq:["$studentId","$$studentId"]
                                    }
                                }
                             },
                             {
                                 $project:{
                                     name:1,
                                     id:1,
                                     studentId:1
                                 }
                             }
                         ],
                         as: "users"
                         }
                    },
                    {
                        $unwind:"$users"
                    },
                    {
                        $match: { cronStatus: '0'
                   }
               },
                    {
                        $project: {
                            studentId:1,
                            id:1,
                            attended:{
                                $cond: { if: { $eq: ['$attended', '1'] }, then: 'Present', else: 'Absent' }
                            },
                            cronStatus:1,
                            studentName:"$users.name"
                        }
                    }  
                    
            ]).
               exec((err,data) => {
                   if(err) {
                    apiErrLog(config.errFolderPath,err,'cronLog',today)   
                    process.kill(process.pid, 'SIGINT');
                   }
                let today = new Date();
                let dd = today.getDate();
                let month = today.getMonth()+1; 
                let mm = month < 9 ? "0" + (month) : month
                let yyyy = today.getFullYear();
                var fileName = 'attendance'+dd+mm+yyyy+'.csv';
                var date = dd+'-'+mm+'-'+yyyy;
                if(data.length > 0){
                    const d = data.map((object, key) => {
                        delete object._id;
                        delete object.cronStatus;
                        return object
                      })
                    stringify(d, {       
                        header: true
                    },  function (err, output) {
                        if(err) {
                            apiErrLog(config.errFolderPath,err,'cronLog',today)
                            process.kill(process.pid, 'SIGINT');
                        }
                        fs.writeFile(process.cwd()+'/public/uploads/'+fileName, output, async (err) => {
                            if(err) {
                                apiErrLog(config.errFolderPath,err,'cronLog',today)
                                process.kill(process.pid, 'SIGINT');
                            }
                        else {
                            const a = await studentAttendance.updateMany({ date: date }, { cronStatus: '0' });
                            process.kill(process.pid, 'SIGINT');
                        }
                        });
                    });
                }
               });

    }catch(err){
        if(err) {
            apiErrLog(config.errFolderPath,err,'cronLog',today)
            process.kill(process.pid, 'SIGINT');}
    }
}


process.on('message', function(m) {
    console.log('Child process received:', m);
    attendanceCron();
  });
    
 