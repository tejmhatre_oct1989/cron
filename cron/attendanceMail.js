import {} from '../helpers/init_mongodb';
import mailer from '../helpers/nodemailer';
import { apiErrLog } from '../config/errFun'
import config from '../config/config';

 function attendanceMailCron(){
    console.log(6)
    var date = config.date();
    let today = date.dd+date.mm+date.yyyy;  
    try{
        console.log(7)
        var fileName = 'attendance'+today+'.csv';

        let to = 'tejmhatre_oct30@outlook.com';
        let subject = 'Sending Email using Node.js';
        let text = 'PFA';
        let attachments = [
        {
            filename: fileName,
            path: process.cwd()+'/public/uploads/'+fileName,
        }
        ];
    
        mailer(to,subject,text,attachments).then((status) => { 
            apiErrLog(config.errFolderPath,{'status':200,message:'mail send successfully'},'cronLog',today,(status) => {
                if(status){
                    console.log(5)
                    process.kill(process.pid, 'SIGINT');
                }
            })   
        }).catch((err) => {
            console.log(2)
            apiErrLog(config.errFolderPath,err,'cronLog',today,(status) => {
                console.log(3)
                if(status){
                    console.log(4)
                    process.kill(process.pid, 'SIGINT');
                }
            })   
        });

    }catch(err){
        process.send(err);
        apiErrLog(config.errFolderPath,err,'cronLog',today,(status) => {
            console.log(1)
            if(status){
                process.kill(process.pid, 'SIGINT');
            }
        })   
        
    }
}



process.on('message', function(m) {
    console.log('Child process received:', m);
    attendanceMailCron();
  });
