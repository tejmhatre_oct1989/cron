import express from 'express';
import morgan from 'morgan';
import createError from 'http-errors';
import {} from './helpers/init_mongodb';
import {} from './helpers/cron';
import route from './routes/route'
import fs from 'fs';
import path from 'path';
import config from './config/config';
import logger from './config/pLogger';

const app = express();

const PORT = process.env.PORT || 3000;

var date = config.date();
let today = date.dd+date.mm+date.yyyy;

var accessLogStream = fs.createWriteStream(path.join(config.errFolderPath+'/access', today+'.log'), { flags: 'a' })
app.use(morgan('combined', { stream: accessLogStream }))


app.use(express.json());
app.use(express.urlencoded({extended:true}))

app.get('/',(req,res) => {
    res.status(200).json({
        'message':'Welcome To first page'
    });
})

app.use('/api',route);

app.use((req,res,next) => {
    next(createError.NotFound('Page Not Found'));
})

app.use((err,req,res,next) => {
    const error = {
        'status': err.status || 500,
        'message': err.message
    }
    logger.info(err.message)
    res.status(err.status || 500);
    res.send(error)
})

app.listen(PORT,() => {
    console.log('Port Listen '+PORT);
})