import express from 'express';
import { uploadstudents,uploadstudentsAttendance,cronForCsv,cronForCsvMail } from '../controller/UserManagement'
import { imageCsv } from '../helpers/multer'



const router = express.Router();


router.post('/import-students',imageCsv.single('file'),uploadstudents);
router.post('/import-students-attendance',imageCsv.single('file'),uploadstudentsAttendance);

router.get('/cron-for-csv',cronForCsv);
router.get('/cron-for-csv-mail',cronForCsvMail);


//router.post('/import-attendence',attendence);

export default router;