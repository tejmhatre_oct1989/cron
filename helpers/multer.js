import multer from 'multer';
import path from 'path';


export const imageCsv = multer({
    storage: multer.diskStorage({
        destination: './public/uploads/', // Destination to store video 
        filename: (req, file, cb) => {
            cb(null, file.fieldname + '_' + Date.now() 
             + path.extname(file.originalname))
        }
    }),
    limits: {
      fileSize: 1000000 // 1000000 Bytes = 1 MB
    },
    fileFilter(req, file, cb) {
      if (!file.originalname.match(/\.(csv|png)$/)) { 
         return cb(new Error('Please upload a csv file'))
       }
     cb(undefined, true)
  }
}) 