import fs from 'fs';
const csv = require('csv-parser')
const stringify = require('csv-stringify')

export function students(filePath,model,cb){

    const users = [];
    fs.createReadStream(filePath)
        .pipe(csv())
        .on('data', function (row) {
            users.push(row);
        })
        .on('end', function () {
            model['model'].insertMany(users, function(err, docs) {
                    if(err) cb(false);
                    cb(true);
            });
            })

}