import nodemailer from 'nodemailer';
import config from '../config/config'

var transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    secureConnection: true,
    port: 465,
    transportMethod: 'SMTP',
    auth: {
      user: config.mail.user,
      pass: config.mail.pass
    }
  });

const mailer = (to,subject,html,attachment) => {

    return new Promise((resolve, reject) =>
    {
        transporter.sendMail(
            {
                from: config.mail.user,
                to: to,
                subject: subject,
                text: html,
                 attachments: attachment
            },
            (err, info) => {
                if (err)
            {
                reject(err);
            }
            else
            {
                resolve(true);
            }

            }
            );   
            
    });

  }
  

  export default mailer;