import mongoose from 'mongoose'
import config from '../config/config';

mongoose.connect(config.dbUrl,{
    useNewUrlParser: true, 
    useUnifiedTopology: true,
    useFindAndModify:false,
    useCreateIndex:true
})
.then(() => {
    console.log('MongoDB Connected');
}).catch((err) => console.log('MongoDB Error - ',err.message)) 