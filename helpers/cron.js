import cron from 'node-cron'
import { cronForCsv, cronForCsvMail } from '../controller/UserManagement'

cron.schedule('00 22 * * *', cronForCsv);
cron.schedule('05 22 * * *', cronForCsvMail);