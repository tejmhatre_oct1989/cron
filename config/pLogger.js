import {createLogger, format, transports} from 'winston'
import config from '../config/config';
const { timestamp, combine, errors, json} = format;
var date = config.date();
let today = date.dd+date.mm+date.yyyy;
function buildProdLogger(){
    return createLogger({
        format: combine(
            timestamp(),
            errors({stack:true}),
            json()
        ),
        defaultMeta:{service:'user-service'},
        transports:[
            new transports.Console(),
            new transports.File({
                filename: config.errFolderPath+'/apiLog/'+today+'.log',
                level: 'info'
              })
        ]
      });
}

  export default buildProdLogger();