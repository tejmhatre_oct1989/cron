const config = {
    dbUrl:"mongodb://localhost:27017/cron",
    mail:{
        user: 'mightycron1@gmail.com',
        pass: 'Crontest@1'
    },
    date: () => {
         let today = new Date();
         let dd = today.getDate();
         let month = today.getMonth()+1;
         let mm =  month < 9 ? "0" + (month) : month;
         let yyyy = today.getFullYear()
         return {
             dd,mm,yyyy
         }
    },
    errFolderPath:__dirname+'/../../errorLog3000'

}


export default config;